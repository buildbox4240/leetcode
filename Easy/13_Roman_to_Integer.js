/**
 * @param {string} s
 * @return {number}
 */
var romanToInt = function(s) {
    let romanNums = {
        'I': 1,
        'V': 5,
        'X': 10,
        'L': 50,
        'C': 100,
        'D': 500,
        'M': 1000
    };
    var total = 0;
    for(let i = 0; i < s.length; i++) {
        let char = s.charAt(i);
        let nextChar = s.charAt(i+1);
        let num = romanNums[char];
        let nextNum = romanNums[nextChar];

        let numberToAdd;
        if(nextNum > num) {
            numberToAdd = nextNum - num;
            i++;
        } else {
            numberToAdd = num;
        }
        total = total+numberToAdd;
    }

    return total;
