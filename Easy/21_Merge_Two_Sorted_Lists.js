/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var mergeTwoLists = function(l1, l2) {
    var combinedList = new ListNode(0);
    var current = combinedList;

    while(l1) {
            while(l2 && (l2.val <= l1.val)) {
                current.next = l2;
                l2 = l2.next;
                current = current.next;
            }
        current.next = l1;
        l1 = l1.next;
        current = current.next
    }

    current.next = l1 || l2;
    return combinedList.next;
};
