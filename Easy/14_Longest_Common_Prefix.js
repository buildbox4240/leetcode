/**
 * @param {string[]} strs
 * @return {string}
 */
var longestCommonPrefix = function(strs) {
    if(strs.length == 0) {
        return "";
    }
    let prefix = strs[0];
    strs.forEach(function(s) {
        if (prefix === ""){ return "" };
        for(let i = 0; i < prefix.length; i++) {
            if(s.charAt(i) !== prefix.charAt(i)) {
                prefix = prefix.substring(0, i);
            }
        }
    });
    return prefix;
};
