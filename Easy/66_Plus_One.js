/**
 * @param {number[]} digits
 * @return {number[]}
 */
var plusOne = function(digits) {
    for(var i = digits.length - 1; i >= 0; i--){
        if(digits[i] < 9) {
            digits[i] = digits[i] + 1;
            break;
        } else {
            digits[i] = 0;
        }
    }

    if(digits[0] === 0) {
        digits[0] = 1;
        digits.push(0);
    }
    return digits;
};
