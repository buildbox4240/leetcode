/**
 * @param {number} x
 * @return {number}
 */
var reverse = function(x) {
    const max = Math.pow(2, 31);
    let rev = 0;
    while(x != 0) {
        let pop = x % 10;
        x = parseInt(x/10, 10);
        rev = rev * 10 + pop;
        if (rev > max || rev < -max) return 0;
    }

    if(x < 0) {
        return -rev;
    } else {
        return rev;
    }
};
