/**
 * @param {number[]} nums
 * @return {number}
 */
var maxSubArray = function(nums) {
    var maxSoFar = nums[0];
    var maxEndingHere = nums[0];

    for(var i = 1; i < nums.length; i++) {
        maxEndingHere = Math.max(maxEndingHere + nums[i], nums[i]);

        if(maxEndingHere > maxSoFar) {
            maxSoFar = maxEndingHere;
        }
    }

    return maxSoFar;
};
