/**
 * @param {string} haystack
 * @param {string} needle
 * @return {number}
 */
var strStr = function(haystack, needle) {
    if(!needle) {
        return 0;
    }

    if(haystack.length < needle.length) {
        return -1;
    }

    for(var i = 0; i < haystack.length - needle.length + 1; i++) {
        if(haystack.charAt(i) === needle.charAt(0)) {
            var match = true;
            for(var j = 1; j < needle.length; j++) {
                if(haystack.charAt(i+j) !== needle.charAt(j)) {
                    match = false;
                    break;
                }
            }
            if(match) {
                return i;
            }
        }
    }

    return -1;
};
