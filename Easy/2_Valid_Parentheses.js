/**
 * @param {string} s
 * @return {boolean}
 */

var solution = (function() {
    var pairs = {
        ')': '(',
        '}': '{',
        ']': '['
    }

    function checkValid(s) {
        var closureStack = [];
        for(var i = 0; i < s.length; i++) {
            var currentChar = s.charAt(i);
            switch(currentChar) {
                case '(':
                case '{':
                case '[':
                    closureStack.push(currentChar);
                    break;
                case ')':
                case ']':
                case '}':
                    if(!handleCloseChar(currentChar, closureStack)) {
                        return false;
                    }
                    break;
            }
        }
        console.log(closureStack);
        return closureStack.length == 0;
    }

    function handleCloseChar(closeChar, closureStack) {
        var lastChar = closureStack.pop();
        var match = pairs[closeChar];
        if(lastChar !== match) {
            return false;
        } else {
            return true;
        }
    }

    return {
        checkValid: checkValid
    }
}());

var isValid = function(s) {
    return solution.checkValid(s);
}
