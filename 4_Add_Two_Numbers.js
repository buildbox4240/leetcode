/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function(l1, l2) {
    var carryOne = false;
    var sumNode = new ListNode(0);
    var sumCursor = sumNode;
    while(l1 != null || l2 != null) {
        var x = l1 != null ? l1.val : 0;
        var y = l2 != null ? l2.val : 0;

        if(carryOne) {
            x = 1 + x;
        }

        var sum = x + y;

        if(sum > 9) {
            carryOne = true;
            sum = sum % 10;
        } else {
            carryOne = false;
        }

        sumCursor.next = new ListNode(0);
        sumCursor = sumCursor.next;
        sumCursor.val = sum;
        if(l1 != null) {
            l1 = l1.next;
        }
        if(l2 != null) {
            l2 = l2.next
        }
    }

    if(carryOne) {
        var lastNode = new ListNode(1);
        sumCursor.next = lastNode;
    }
    return sumNode.next;
};
